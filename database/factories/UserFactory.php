<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\Model\User;


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'images' => $faker->imageUrl( 640, 480, 'people' ),
        'email_verified_at' => now(),
        'password' => '$2y$10$C35ZzlQ5rVjUtmhNUGTra.16C3VJKio1qLGrdfkWGQClRkzyEqi3C', // secret
        'remember_token' => Str::random(10),
    ];
});
