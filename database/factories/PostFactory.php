<?php

use Faker\Generator as Faker;
use App\Model\Post;

$factory->define(Post::class, function (Faker $faker) {
    return [
		'user_id'          => $faker->numberBetween( 1, 50 ),
		'category_id'      => $faker->numberBetween( 1, 100 ),
		'title'            => $faker->unique()->text( 20 ),
		'slug'             => $faker->unique()->slug(),
		'type'			   => 'product',
		'images'           => 'https://source.unsplash.com/collection/190727/720x680',
		'description'      => $faker->text( 255 ),
		'content'          => $faker->paragraph( 20, true ),
		'status'		   => 'publish',
		'meta_title'       => $faker->text( 255 ),
		'meta_description' => $faker->text( 255 ),
		'meta_keywords'    => $faker->text( 255 )
    ];
});
