<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->smallInteger('category_id')->unsigned();
            $table->string('title');
            $table->string('slug')->nullable();
            $table->string('images')->nullable();
            $table->char('type', 10)->default('post');
            $table->text('description')->nullable();
            $table->longText('content')->nullable();
            $table->char('status', 10);

            // meta
            $table->char('meta_title', 255)->nullable();
            $table->char('meta_description', 255)->nullable();
            $table->char('meta_keywords', 255)->nullable();

            // make index
            $table->index( 'slug' );

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
