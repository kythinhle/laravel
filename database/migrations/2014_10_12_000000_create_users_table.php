<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('permission', 20)->default('General User');
            $table->string('images')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->tinyInteger('active')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });

        // Create admin account
        $create = DB::table('users')->insert( [
            'name' => 'Kỳ Thịnh Lê',
            'email' => 'kythinhle@gmail.com',
            'permission'  => 'Supper Admin',
            'images' => 'https://lh3.googleusercontent.com/-a_UGEqMw-Uo/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rdoU5ZdiAZ5CZ5kAgSx8r35806xLQ/s32-c-mo/photo.jpg',
            'password' => bcrypt( 'Thinh@123' ),
            'active' => 1
        ] );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
