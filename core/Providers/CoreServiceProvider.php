<?php

namespace Core\Providers;

use Illuminate\Support\ServiceProvider;
use Core\Repositories\Post\PostRepositoryContract;
use Core\Repositories\Post\PostRepository;
use Core\Repositories\Product\ProductRepositoryContract;
use Core\Repositories\Product\ProductRepository;
use Core\Services\Post\PostServiceContract;
use Core\Services\Post\PostService;
use Core\Services\Product\ProductServiceContract;
use Core\Services\Product\ProductService;

class CoreServiceProvider extends ServiceProvider
{   
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {   
        # post repository contract
        $this->app->singleton( PostRepositoryContract::class, PostRepository::class );
        # post services contract
        $this->app->singleton( PostServiceContract::class, PostService::class );

        # product repository contract
        $this->app->singleton( ProductRepositoryContract::class, ProductRepository::class );
        # product services contract
        $this->app->singleton( ProductServiceContract::class, ProductService::class );
    }
}
