<?php

namespace Core\Repositories\Post;

interface PostRepositoryContract {
	public function paginate($quantity);
    public function find($id);
    public function store($data);
    public function update($id, $data);
    public function destroy($id);
}