<?php

namespace Core\Repositories\Post;

use Illuminate\Support\Facades\DB;
use App\Model\Post;

/**
 * Post Repository
 */
class PostRepository implements PostRepositoryContract
{
	/**
	 * @var object $model
	 */
	protected $model;
	
	public function __construct( Post $model )
	{
		$this->model = $model;
	}

	public function index( $quantity ) {
		return $data = DB::table( 'posts' )->where( [ 'type' => 'post', 'publish' => 1 ] )->paginate( $quantity );
	}

	public function paginate( $quantity = 10 ) {
		return $this->model->paginate( $quantity );
	}

	public function find( $id ) {
		return $this->model->findOrFail( $id );
	}

	public function store( $data ) {
		return $this->model->create( $data );
	}

	public function update( $id, $data ) {
		$model = $this->find( $id );
		return $model->update( $data );
	}

	public function destroy( $id ) {
		$model = $this->find( $id );
		return $model->destroy( $id );
	}
}