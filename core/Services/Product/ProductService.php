<?php

namespace Core\Services\Product;

use Core\Repositories\Product\ProductRepositoryContract;

/**
 * Product Service
 */
class ProductService implements ProductServiceContract
{
	/**
	 * @var Object Products repository
	 */
	protected $repository;

	public function __construct( ProductRepositoryContract $repository )
	{
		$this->repository = $repository;
	}

	public function paginate( $quantity = 10 ) {
		return $this->repository->paginate( $quantity );
	}

	public function find( $id ) {
		return $this->repository->findOrFail( $id );
	}

	public function store( $data ) {
		# handle data
		$product = $this->repository->create( $data );
		return $product;
	}

	public function update( $id, $data ) {
		return $this->repository->update($id, $data);
	}

	public function destroy( $id ) {
		return $this->repository->destroy($id);
	}
}