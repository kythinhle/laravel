<?php

namespace Core\Services\Post;

use Core\Repositories\Post\PostRepositoryContract;

/**
 * Post Service
 */
class PostService implements PostServiceContract
{
	/**
	 * @var Object posts repository
	 */
	protected $repository;

	public function __construct( PostRepositoryContract $repository )
	{
		$this->repository = $repository;
	}

	public function index( $quantity ) {
		return $this->repository->index( $quantity );
	}

	public function paginate( $quantity = 10 ) {
		return $this->repository->paginate( $quantity );
	}

	public function find( $id ) {
		return $this->repository->findOrFail( $id );
	}

	public function store( $data ) {
		return $this->repository->create( $data );
	}

	public function update( $id, $data ) {
		return $this->repository->update($id, $data);
	}

	public function destroy( $id ) {
		return $this->repository->destroy($id);
	}
}