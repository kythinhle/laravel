<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group( ['prefix' => 'v1', 'namespace' => 'API', 'middleware' => 'cors'], function() {

	Route::post( 'login', 'UserController@login' )->name( 'login' );

	Route::group( ['middleware' => 'auth:api'], function() {
		Route::group( ['middleware' => 'permission'], function() {
			
			// Route::group( ['prefix' => 'products'], function() {
			// 	Route::post( 'create', 'ProductController@store' );
			// } );
			# posts
			Route::resource( 'posts', 'PostController' );
		} );
	} );

	# products
	Route::group( ['prefix' => 'products'], function() {
		Route::get( '/', 'ProductController@index' );
	} );

	# CSRF
	Route::get( 'csrf', function() {
		return csrf_token();
	} )->middleware( 'web' );
} );