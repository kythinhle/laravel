const copyWebpackPlugins = require('copy-webpack-plugin')

module.exports = {
	outputDir: '../public',
	indexPath: '../resources/views/admin/index.blade.php',

	pages: {
		index: {
			entry: 'src/index.js'
		}
	},
	devServer: {
		proxy: 'http://localhost/laravel/'
	},
	// modifying options of loader
	chainWebpack: config => {
		config.module
			.rule('vue')
			.use('vue-loader')
			.loader('vue-loader')
			.tap(options => {
				options.compilerOptions.preserveWhitespace = true
				return options
			})
	},

	productionSourceMap: false,
	configureWebpack: {
		plugins: [
			new copyWebpackPlugins([
				{ from: 'src/assets/img/', to: '../public/img/' },
				{ from: 'src/assets/img/svg/', to: '../public/img/svg/' },
				{ from: 'src/assets/fonts/', to: '../public/fonts' }
			])
		]
	}

}