import axios from 'axios'

import { base_url, api_version, api_endpoint } from '../config'

export default axios.create({
	baseURL: base_url + api_endpoint + api_version
})