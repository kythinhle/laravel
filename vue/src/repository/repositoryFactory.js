import ProductRepository from './productsRepository.js'

const repositories = {
	products: ProductRepository
}

export const Factory = {
	get: name => repositories[name]
}