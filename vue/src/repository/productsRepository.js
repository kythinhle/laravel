import repository from './service'

const endpoint = '/products/'

export default {
	get() {
		return repository.get(`${endpoint}`)
	}
}