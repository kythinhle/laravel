import vue from 'vue'
import router from 'vue-router'

import store from './store/index'

vue.use(router)

const Router = new router({
	mode: 'history',
	linkActiveClass: 'active',
	routes: [
		{
			path: '/',
			name: 'Home',
			redirect: '/dashboard',
			component: () => import(/* webpackChunkName: "master" */ './views/app'),
			beforeEnter: (to, from, next) => {
				let currentUser = store.getters.currentUser
				if (typeof currentUser !== undefined && currentUser.access_token) {
					next()
					return
				}
				next({ name: 'Login' })
			},
			children: [
				{
					path: '/dashboard',
					name: 'Dashboard',
					component: () => import(/* webpackChunkName: "dashboard" */ './views/app/pages/dashboard')
				},
				{
					path: '/e-commerce',
					name: 'E-Commerce',
					redirect: '/e-commerce/analytics',
					component: () => import(/* webpackChunkName: "ecommerce" */ './views/app/pages/ecommerce'),
					children: [
						{
							path: '/e-commerce/analytics',
							name: 'E-Commerce-Dashboard',
							component: () => import(/* webpackChunkName: "ecommerce" */ './views/app/pages/ecommerce/dashboard')
						},
						{
							path: '/e-commerce/products',
							name: 'E-Commerce-Products',
							component: () => import(/* webpackChunkName: "ecommerce" */ './views/app/pages/ecommerce/products')
						}
					]
				}
			]
		},
		{
			path: '/login',
			name: 'Login',
			component: () => import(/* webpackChunkName: "login" */ './views/app/auth/login'),
			beforeEnter: (to, from, next) => {
				let currentUser = store.getters.currentUser
				if (currentUser) {
					next({ name: 'Home' })
					return
				}
				next()
			}
		}
	]
})

export default Router