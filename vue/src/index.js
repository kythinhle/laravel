import './assets/css/vendor/bootstrap.min.css'

var color = 'light.orange'

// get storage theme color
if (localStorage.getItem('themeColor')) {
	color = localStorage.getItem('themeColor')
}

let render = () => {
	import('./assets/css/sass/themes/piaf.' + color + '.scss').then(x => require('./main'))
}

// init theme
render()