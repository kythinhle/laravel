import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import VueLazyload from 'vue-lazyload'

import router from './router'
import App from './App'
import store from './store'

// perfect scrollbar
import vuePerfectScrollbar from 'vue-perfect-scrollbar'

// Colxx components additions
import Colxx from './components/common/Colxx'

// external css
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/fonts/simple-line-icons/css/simple-line-icons.css'
import './assets/fonts/iconsmind-s/css/iconsminds.css'

// integrate lazyload
Vue.use(VueLazyload, {
	preLoad: 0,
})

Vue.use(BootstrapVue)
Vue.config.productionTip = false

// register global components
Vue.component('vue-perfect-scrollbar', vuePerfectScrollbar)
Vue.component('b-colxx', Colxx)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
