import MenuIcon from './MenuIcon'
import MobileMenuIcon from './MobileMenuIcon'
import DataListIcon from './DataListIcon'
import ImageListIcon from './ImageListIcon'
import ThumbListIcon from './ThumbListIcon'

export { MenuIcon, MobileMenuIcon, DataListIcon, ImageListIcon, ThumbListIcon }