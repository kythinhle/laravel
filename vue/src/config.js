export const defaultMenuType = 'menu-default'

export const subHiddenBreakpoint = 1440
export const menuHiddenBreakpoint = 768

export const base_url = 'http://192.168.3.114/laravel/'
export const api_version = 'v1/'
export const api_endpoint = 'api/'