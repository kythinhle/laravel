import axios from '../../repository/service'

const state = {
	current_user: localStorage.getItem('user') || null
}
const getters = {
	currentUser: state => state.current_user ? JSON.parse(state.current_user) : null
}
const mutations = {
	store_user: (state, payload) => {
		state.current_user = payload
	}
}
const actions = {
	async doLogin ({ commit }, payload) {
		let response
		try {
			response = await axios.post('/v1/login', { ...payload })
		} catch(e) {
			// statements
			console.log(e);
			return
		}
		const data = response.data
		if (data) {
			let parse_data = JSON.stringify(data)
			localStorage.setItem('user', parse_data)
			commit('store_user', parse_data)
		}
	}
}

export default {
	state,
	getters,
	mutations,
	actions
}