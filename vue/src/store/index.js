import Vue from 'vue'
import Vuex from 'vuex'

// load modules
import sidebar from './modules/sidebar'
import user from './modules/user'

Vue.use(Vuex)

const store = new Vuex.Store({
	modules: {
		sidebar,
		user
	}
})

export default store