const mix = require('laravel-mix');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/src/index.js', 'public/src/vue')

mix.webpackConfig({
   mode: 'development',
   output: {
      path: __dirname + '/public/src/dist/'
   },
   module: {
      rules: [
         {
            test: /\.scss$/,
            include: [
               path.resolve(__dirname, '/public/src/dist/css/')
            ]
         }
      ]
   }
});


	
	// .copyDirectory('resources/assets/images', 'public/assets/admin/images')
	// .copyDirectory('resources/assets/admin/fonts', 'public/assets/admin/fonts')

   // .styles([
   // 		'resources/assets/admin/css/jquery-jvectormap-2.0.2.css',
   // 		'resources/assets/admin/css/simplebar.css',
   // 		'resources/assets/admin/css/bootstrap.min.css',
   // 		'resources/assets/admin/css/animate.css',
   // 		'resources/assets/admin/css/icons.css',
   // 		'resources/assets/admin/css/sidebar-menu.css',
   // 		'resources/assets/admin/css/app-style.css',
   //       // 'resources/assets/admin/css/dataTables.bootstrap4.min.css'
   // 	], 'public/assets/admin/css/library.min.css')

   // .scripts([
   // 		// 'resources/assets/admin/template-js/jquery.min.js',
   // 		// 'resources/assets/admin/template-js/popper.min.js',
   // 		// 'resources/assets/admin/template-js/bootstrap.min.js',
   //       // 'resources/assets/admin/template-js/simplebar.js',
   //       // 'resources/assets/admin/template-js/sidebar-menu.js',
   // 		'resources/assets/admin/template-js/app-script.js',
   // 		// 'resources/assets/admin/template-js/Chart.min.js',
   // 		// 'resources/assets/admin/template-js/jquery-jvectormap-2.0.2.min.js',
   // 		// 'resources/assets/admin/template-js/jquery-jvectormap-world-mill-en.js',
   // 		// 'resources/assets/admin/template-js/jquery.sparkline.min.js',
   // 		// 'resources/assets/admin/template-js/excanvas.js',
   // 		// 'resources/assets/admin/template-js/jquery.knob.js',
   //   //     'resources/assets/admin/template-js/jquery.dataTables.min.js',
   //   //     'resources/assets/admin/template-js/dataTables.bootstrap4.min.js',
   //   //     'resources/assets/admin/template-js/dataTables.buttons.min.js',
   // 		// 'resources/assets/admin/template-js/index.js',
   // 	], 'public/assets/admin/js/library.min.js')
