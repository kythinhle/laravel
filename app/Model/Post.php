<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'category_id', 'title', 'slug', 'type', 'images', 'description', 'content', 'meta_title', 'meta_description', 'meta_keywords'
    ];

    /**
     * The relationships that should always be loaded.
     * 
     * @var array
     */
    protected $with = ['post_meta'];

    # relationships
    public function post_meta() {
    	return $this->hasMany( 'App\Model\Posts_Meta', 'post_id' );
    }

    public function user() {
    	return $this->belongsTo( 'App\Model\User' );
    }
}
