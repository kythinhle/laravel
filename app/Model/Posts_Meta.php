<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Posts_Meta extends Model
{
    protected $table = 'posts_meta';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'post_id', 'key', 'value'
    ];

    public $timestamps = false;

    # relationships
    public function post() {
    	return $this->belongsTo( 'App\Model\Post' );
    }
}
