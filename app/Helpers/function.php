<?php

/**
 * Convert a value to non-negative integer.
 * 
 * @param  mixed $maybeint Data you wish to have converted to a non-negative integer.
 * @return int A non-negative integer.
 */
function absint( $maybeint ) {
	return abs( intval( $maybeint ) );
}

/**
 * Add hooks callback here
 */
