<?php

// Initialize the filter globals.
require_once dirname( __FILE__ ) . '/class-hook.php';

/**
 * @var Core_Hook $core_filter
 */
global $core_filter, $core_actions, $core_current_filter;

if ( $core_filter ) {
	$core_filter = Core_Hook::build_preinitialized_hooks( $core_filter );
} else {
	$core_filter = array();
}

if ( ! isset( $core_actions ) ) {
	$core_actions = array();
}

if ( ! isset( $core_current_filter ) ) {
	$core_current_filter = array();
}

/**
 * Call the 'all' hook, which will process the functions hooked into it.
 *
 * The 'all' hook passes all of the arguments or parameters that were used for
 * the hook, which this function was called for.
 *
 * This function is used internally for apply_filters(), do_action(), and
 * do_action_ref_array() and is not meant to be used from outside those
 * functions. This function does not check for the existence of the all hook, so
 * it will fail unless the all hook exists prior to this function call.
 * 
 * @global array $wp_filter  Stores all of the filters
 *
 * @param array $args The collected parameters from the hook that was called.
 */
function _core_call_all_hook( $args ) {
	global $core_filter;

	$core_filter['all']->do_all_hook( $args );
}

/**
 * Hook a function or method to a specific filter action.
 *
 * @global array $core_filter     A multidimensional array of all hooks and the callbacks hooked to them.
 * @param string 	$tag             The name of the filter to hook the $function_to_add callback to.
 * @param callable 	$function_to_add The callback to be run when the filter is applied.
 * @param int 		$priority        Optional. Used to specify the order in which the functions
 *                                   associated with a particular action are executed. Default 10.
 *                                   Lower numbers correspond with earlier execution,
 *                                   and functions with the same priority are executed
 *                                   in the order in which they were added to the action.
 * @param int 		$accepted_args   Optional. The number of arguments the function accepts. Default 1.
 * @return true
 */
function add_filter( $tag, $function_to_add, $priority, $accepted_args ) {
	global $core_filter;
	if ( ! isset( $core_filter[$tag] ) ) {
		$core_filter[$tag] = new Core_Hook();
	}
	$core_filter[$tag]->add_filter( $tag, $function_to_add, $priority, $accepted_args );
	return true;
}

/**
 * Call the functions added to a filter hook.
 * 
 * @param  string $tag   The name of the filter hook.
 * @param  mixed  $value The value on which the filters hooked to `$tag` are applied on.
 * @return mixed  $var   Additional variables passed to the functions hooked to `$tag`.
 * @return mixed The filtered value after all hooked functions are applied to it.
 */
function apply_filter( $tag, $value ) {
	global $core_filter, $core_current_filter;

	$args = array();
	
	# do 'all' actions first
	if ( isset( $core_filter['all'] ) ) {
		$core_current_filter[] = $tag;
		$args = func_get_args();
		_core_call_all_hook( $args );
	}

	if ( ! isset( $core_filter[$tag] ) ) {
		if ( isset( $core_filter['all'] ) ) {
			array_pop( $core_current_filter );
		}
		return $value;
	}

	if ( empty( $args ) ) {
		$args = func_get_args();
	}

	if ( empty( $args ) ) {
		$args = func_get_args();
	}

	array_shift( $args );

	# call action hook
	$filtered = $core_filter[$tag]->apply_filter( $value, $args );
	
	array_pop( $core_current_filter );

	return $filtered;
}

/**
 * Check if any filter has been registered for a hook.
 *
 * @global array $core_filter Stores all of the filters.
 *
 * @param string        $tag               The name of the filter hook.
 * @param callable|bool $function_to_check Optional. The callback to check for. Default false.
 * @return false|int If $function_to_check is omitted, returns boolean for whether the hook has
 *                   anything registered. When checking a specific function, the priority of that
 *                   hook is returned, or false if the function is not attached. When using the
 *                   $function_to_check argument, this function may return a non-boolean value
 *                   that evaluates to false (e.g.) 0, so use the === operator for testing the
 *                   return value.
 */
function has_filter( $tag, $function_to_check = false ) {
	global $core_filter;

	if ( ! isset( $core_filter[$tag] ) ) {
		return false;
	}

	return $core_filter[$tag]->has_filter( $tag, $function_to_check );
}

/**
 * Removes a function from a specified filter hook.
 * 
 * @param string   $tag                The filter hook to which the function to be removed is hooked.
 * @param callable $function_to_remove The name of the function which should be removed.
 * @param int      $priority           Optional. The priority of the function. Default 10.
 * 
 * @return bool    Whether the function existed before it was removed.
 */
function remove_filter( $tag, $function_to_remove, $priority = 10 ) {
	global $core_filter;

	$r = false;

	if ( isset( $core_filter[$tag] ) ) {
		$r = $core_filter[$tag]->remove_filter( $tag, $function_to_remove, $priority );
		if ( ! $core_filter[$tag]->callbacks ) {
			unset( $core_filter[$tag] );
		}
	}

	return $r;
}

/**
 * Hooks a function on to a specific action.
 * 
 * @param string   $tag             The name of the action to which the $function_to_add is hooked.
 * @param callable $function_to_add The name of the function you wish to be called.
 * @param int      $priority        Optional. Used to specify the order in which the functions
 *                                  associated with a particular action are executed. Default 10.
 *                                  Lower numbers correspond with earlier execution,
 *                                  and functions with the same priority are executed
 *                                  in the order in which they were added to the action.
 * @param int      $accepted_args   Optional. The number of arguments the function accepts. Default 1.
 * @return true Will always return true.
 */
function add_action( $tag, $function_to_add, $priority = 10, $accepted_args = 1 ) {
	return add_filter( $tag, $function_to_add, $priority, $accepted_args );
}

/**
 * Execute functions hooked on a specific action hook.
 *
 * This function invokes all functions attached to action hook `$tag`. It is
 * possible to create new action hooks by simply calling this function,
 * specifying the name of the new hook using the `$tag` parameter.
 *
 * You can pass extra arguments to the hooks, much like you can with apply_filters().
 * 
 * @global array $wp_filter         Stores all of the filters
 * @global array $wp_actions        Increments the amount of times action was triggered.
 * @global array $wp_current_filter Stores the list of current filters with the current one last
 *
 * @param string $tag     The name of the action to be executed.
 * @param mixed  $arg,... Optional. Additional arguments which are passed on to the
 *                        functions hooked to the action. Default empty.
 */
function do_action( $tag, $arg = '' ) {
	global $core_filter, $core_actions, $core_current_filter;

	if ( ! isset( $core_actions[$tag] ) ) {
		$core_actions[$tag] = 1;
	} else {
		++$core_actions[$tag];
	}

	# do 'all' actions first
	if ( isset( $core_filter['all'] ) ) {
		$core_current_filter[] = $tag;
		$all_args = func_get_args();
		_core_call_all_hook( $all_args );
	}

	if ( ! isset( $core_filter[$tag] ) ) {
		if ( isset( $core_filter['all'] ) ) {
			array_pop( $core_current_filter );
		}
		return;
	}

	if ( ! isset( $core_filter['all'] ) ) {
		$core_current_filter[] = $tag;
	}

	$args = array();

	if ( is_array( $arg ) and 1 == count($arg) and isset( $arg[0] ) and is_object( $arg[0] ) ) {
		$args[] =& $arg[0];
	} else {
		$args[] = $arg;
	}

	for ( $a = 2; $a < func_num_args(); $a++ ) { 
		$args[] = func_get_arg($a);
	}

	$core_filter[$tag]->do_action( $args );

	array_pop( $core_current_filter );
}

/**
 * Check if any action has been registered for a hook.
 *
 * @see has_filter() has_action() is an alias of has_filter().
 * 
 * @param string        $tag               The name of the action hook.
 * @param callable|bool $function_to_check Optional. The callback to check for. Default false.
 * @return bool|int If $function_to_check is omitted, returns boolean for whether the hook has
 *                  anything registered. When checking a specific function, the priority of that
 *                  hook is returned, or false if the function is not attached. When using the
 *                  $function_to_check argument, this function may return a non-boolean value
 *                  that evaluates to false (e.g.) 0, so use the === operator for testing the
 *                  return value.
 */
function has_action( $tag, $function_to_check = false ) {
	return has_filter($tag, $function_to_check);
}

/**
 * Removes a function from a specified action hook.
 *
 * This function removes a function attached to a specified action hook. This
 * method can be used to remove default functions attached to a specific filter
 * hook and possibly replace them with a substitute.
 *
 * @param string   $tag                The action hook to which the function to be removed is hooked.
 * @param callable $function_to_remove The name of the function which should be removed.
 * @param int      $priority           Optional. The priority of the function. Default 10.
 * @return bool Whether the function is removed.
 */
function remove_action( $tag, $function_to_remove, $priority = 10 ) {
	return remove_filter( $tag, $function_to_remove, $priority );
}

/**
 * Build Unique ID for storage and retrieval.
 * 
 * @param  string 	$tag      Used in counting how many hooks were applied
 * @param  callable $function Used for creating unique id
 * @param  int|bool $priority Used in counting how many hooks were applied. If === false
 *                            and $function is an object reference, we return the unique
 *                            id only if it already has one, false otherwise.
 * @return string|false       Unique ID for usage as array key or false if $priority === false
 *                            and $function is an object reference, and it does not already have
 *                            a unique id.
 */
function _core_filter_build_unique_id( $tag, $function, $priority ) {
	global $core_filter;
	static $filter_id_count = 0;

	if ( is_string( $function ) ) {
		return $function;
	}

	if ( is_object( $function ) ) {
		$function = array( $function, '' );
	} else {
		$function = (array) $function;
	}

	if ( is_object( $function[0] ) ) {
		if ( function_exists( 'spl_object_hash' ) ) {
			return spl_object_hash( $function[0] . $function[1] );
		} else {
			$obj_idx = get_class( $function[0] ) . $function[1];
			if ( ! isset( $function[0]->core_filter_id ) ) {
				if ( false === $priority ) {
					return false;
				}
				$obj_idx .= isset( $core_filter[$tag][$priority] ) ? count( (array) $core_filter[$tag][$priority] ) : $filter_id_count;
				$function[0]->core_filter_id = $filter_id_count;
				++$filter_id_count;
			} else {
				$obj_idx .= $function[0]->core_filter_id;
			}

			return $obj_idx;
		}
	} elseif ( is_string( $function[0] ) ) {
		return $function[0] . '::' . $function[1];
	}
}
