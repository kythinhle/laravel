<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;
use Core\Services\Product\ProductServiceContract;

class ProductController extends Controller
{
	/**
	 * @var Object Service
	 */
   	protected $service;

   	const cacheTimeout = 10 * 60;

   	public function __construct( ProductServiceContract $service ) {
   		$this->service = $service;
   	}

   	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   	public function index( Request $request, $quantity = 10 ) {

   		$paged = $request->query( 'page' );

   		$data = Cache::remember( 'products-page-' . $paged, self::cacheTimeout, function() use ( $quantity ) {
   			return $this->service->paginate( $quantity );
   		} );

   		return response()->json( $data, 200 );
   	}

   	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request ) {
    	echo '<pre>';print_r( json_decode( $request->getContent(), true ) );echo '</pre>';
        // return json_decode( $request->all(), true );
    }
}
