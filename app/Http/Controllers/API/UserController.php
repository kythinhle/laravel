<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Model\User;

class UserController extends Controller
{
	/**
	 * @var Model User
	 */
	protected $user;

	public function __construct( User $user ) {
		$this->user = $user;
	}

	/**
	 * API - Login
	 * @param  Request $request
	 * @return Auth
	 */
    public function login( Request $request ) {

    	$credentials = $request->all();
    	# Auth user
    	$authentication = Auth::attempt( $credentials );
    	if ( ! $authentication ) {
    		return response()->json( ['message' => 'Unauthorized'], 401 );
    	} else {
    		# Model: User
    		$user = $request->user();
    		$tokenResult = $user->createToken('Personal Access Token');
    		$token = $tokenResult->token;
    		
    		if ( $request->remember_me ) {
    			$token->expires_at = Carbon::now()->addWeeks( 1 );
    		}
    		# update user token
    		$token->save();

            $response = array(
                'token_type'   => 'Bearer',
                'access_token' => $tokenResult->accessToken,
                'expires_at'   => Carbon::parse( $tokenResult->token->expires_at )->toDateTimeString(),
                'user'         => $user,
            );

    		return response()->json( $response, 200 );
    	}
    }
}
