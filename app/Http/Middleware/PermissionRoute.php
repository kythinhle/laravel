<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class PermissionRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( 'General User' == Auth::user()->permission ) {
            return response()->json( ['message' => 'Your account does not have access !'], 401 );
        }
        return $next($request);
    }
}
